﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class JfCustomerCodeAndPelicanCheckNumber
    {
        public string JfCustomerCode { get; set; }
        public string PelicanCheckNumber { get; set; }
        public string NumberEnd { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class PelicanStatusEntity
    {
        public string PelicanStatusCode { get; set; }
        public string ScanItem { get; set; }
        public string ArriveOption { get; set; }
        public bool IsDeliveryD { get; set; }
        public bool IsDeliveryR { get; set; }
        public string EndCode { get; set; }
    }
}

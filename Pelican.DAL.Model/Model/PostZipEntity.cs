﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class PostZipEntity
    {
        public string PostCity { get; set; }
        public string PostArea { get; set; }
        public string Zip { get; set; }
    }
}

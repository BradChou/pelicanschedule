﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class ResidueCheckNumber
    {
        public string Ids { get; set; }
        public int ResidueCheckNumberCount { get; set; }
    }
}

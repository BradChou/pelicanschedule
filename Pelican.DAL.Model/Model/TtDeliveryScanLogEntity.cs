﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class TtDeliveryScanLogEntity
    {
        public string DriverCode { get; set; }
        public string CheckNumber { get; set; }
        public string ScanItem { get; set; }
        public string ScanDate { get; set; }
        public string AreaArriveCode { get; set; }
        public string ArriveOption { get; set; }
        public string CDate { get; set; }

        //以下是為了回覆檔log
        public string ScanLogId { get; set; }
        public string ReturnFilename { get; set; }
        public string AccountCode { get; set; }
        public string PelicanCheckNumber { get; set; }
        public string ReceiveZip3 { get; set; }
        public string InformationCreateDate { get; set; }
        public string ReceiveDate { get; set; }
        public string ReturnFileCreateTime { get; set; }
        public string EndCode { get; set; }
        public string StatusCode { get; set; }
        public string ScanStationCode { get; set; }
        public string ScanStationShortname { get; set; }
        public string StatusChinese { get; set; }
        public string ReceiveCode { get; set; }
        public string AbnormalStatusCode { get; set; }
        public string AbnormalStatusChinese { get; set; }
        public bool IsD { get; set; }       //是否為正物流
    }
}

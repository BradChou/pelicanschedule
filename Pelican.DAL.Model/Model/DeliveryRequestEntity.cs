﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class DeliveryRequestEntity
    {
        public string JFRequestId { get; set; }
        public string JFCheckNumber { get; set; }
        public string PelicanCheckNumber { get; set; }
        public string OrderNumber { get; set; }
        public string Pieces { get; set; }
        public string SendContact { get; set; }
        public string SendTel { get; set; }
        public string SendCity { get; set; }
        public string SendArea { get; set; }
        public string SendAddress { get; set; }
        public string SendZip3 { get; set; }
        public string ReceiveContact { get; set; }
        public string ReceiveTel { get; set; }
        public string ReceiveCity { get; set; }
        public string ReceiveArea { get; set; }
        public string ReceiveAddress { get; set; }
        public string ReceiveZip3 { get; set; }
        public string ArriveAssignDate { get; set; }
        public string AssignedTimePeriod { get; set; }
        public string CollectionMoney { get; set; }
        public string DeliveryType { get; set; }
        public string Remark { get; set; }
        public string JfCustomerCode { get; set; }
        public string PelicanReceiveZip5 { get; set; }
        public string PelicanReceiveArea { get; set; }
        public string RoundTrip { get; set; }
        public string DrRequestId { get; set; }
        public string RoundTripCheckNumber { get; set; }
        public bool IsNeedToInsertTable { get; set; }
    }
}

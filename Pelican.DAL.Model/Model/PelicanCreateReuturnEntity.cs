﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class PelicanCreateReuturnEntity
    {
        public string PZip5 { get; set; }
        public string Area { get; set; }
        public bool IsSuccess { get; set; }
    }
}

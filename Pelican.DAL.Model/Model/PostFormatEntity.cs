﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class PostFormatEntity
    {
        public int Id { get; set; }
        public bool IsNecessary { get; set; }
        public string ColumnName { get; set; }
        public bool IsNumberOnly { get; set; }
        public int StartLocation { get; set; }
        public int EndLocation { get; set; }
        public string Remark { get; set; }
    }
}

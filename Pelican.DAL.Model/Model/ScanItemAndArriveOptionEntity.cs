﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Model
{
    public class ScanItemAndArriveOptionEntity
    {
        public string ScanItem { get; set; }
        public string ArriveOption { get; set; }
    }
}

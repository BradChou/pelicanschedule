﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Condition
{
   public class Notification_Condition
    {
        public long id { get; set; }
        public int Type { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
    }
}

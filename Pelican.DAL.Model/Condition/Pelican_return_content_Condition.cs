﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL.Model.Condition
{
    public class Pelican_return_content_Condition
    {
        public int id { get; set; }
        public string scan_log_id { get; set; }
        public DateTime? write_in_time { get; set; }
        public string return_filename { get; set; }
        public string account_code { get; set; }
        public string pelican_check_number { get; set; }
        public string fse_check_number { get; set; }
        public string receive_zip3 { get; set; }
        public DateTime? information_create_date { get; set; }
        public DateTime? receive_date { get; set; }
        public DateTime? return_file_create_time { get; set; }
        public string end_code { get; set; }
        public string status_code { get; set; }
        public string scan_station_code { get; set; }
        public string scan_station_shortname { get; set; }
        public DateTime? handy_time { get; set; }
        public string status_chinese { get; set; }
        public string receive_code { get; set; }
        public string abnormal_status_code { get; set; }
        public string abnormal_status_chinese { get; set; }

    }
}

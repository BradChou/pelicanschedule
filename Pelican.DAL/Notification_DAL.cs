﻿using Dapper;
using Pelican.DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Pelican.DAL
{
    public class Notification_DAL : Base_DA
    {
        public void Insert(string Subject, string Body)
        {
            Notification_Condition data = new Notification_Condition();
            data.Type = 1;
            data.Body = Body;
            data.Subject = Subject;
            data.CreateUser = "Pelican";

            data.Recipient = Authorization;


            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                   @"  
                    INSERT [dbo].[Notification] 
                    (
       [Type]
      ,[Recipient]
      ,[Subject]
      ,[Body]
      ,[CreateUser]

                    ) VALUES
                    (   
       @Type
      ,@Recipient
      ,@Subject
      ,@Body
      ,@CreateUser

                    )
                    ";
                cn.Execute(sql, data);
            }
        }
    }
}

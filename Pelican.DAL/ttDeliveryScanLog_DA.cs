﻿using Dapper;
using Pelican.DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Pelican.DAL
{
   public class ttDeliveryScanLog_DA : Base_DA
    {
        public string InsertReId(ttDeliveryScanLog_Condition _condition)
        {

            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                    @"  INSERT INTO [dbo].[ttDeliveryScanLog]
           (
            [driver_code]
           ,[check_number]
           ,[scan_item]
           ,[scan_date]
           ,[area_arrive_code]
           ,[platform]
           ,[car_number]
           ,[sowage_rate]
           ,[area]
           ,[ship_mode]
           ,[goods_type]
           ,[pieces]
           ,[weight]
           ,[runs]
           ,[plates]
           ,[exception_option]
           ,[arrive_option]
           ,[sign_form_image]
           ,[sign_field_image]
           ,[deliveryupload]
           ,[photoupload]
           ,[cdate]
           ,[cuser]
           ,[tracking_number]
           ,[Del_status]
           ,[VoucherMoney]
           ,[CashMoney]
           ,[write_off_type]
           ,[receive_option]
           ,[send_option]
           ,[delivery_option]
           ,[option_category]
            )
     VALUES
           (
            @driver_code
           ,@check_number
           ,@scan_item
           ,@scan_date
           ,@area_arrive_code
           ,@platform
           ,@car_number
           ,@sowage_rate
           ,@area
           ,@ship_mode
           ,@goods_type
           ,@pieces
           ,@weight
           ,@runs
           ,@plates
           ,@exception_option
           ,@arrive_option
           ,@sign_form_image
           ,@sign_field_image
           ,@deliveryupload
           ,@photoupload
           ,@cdate
           ,@cuser
           ,@tracking_number
           ,@Del_status
           ,@VoucherMoney
           ,@CashMoney
           ,@write_off_type
           ,@receive_option
           ,@send_option
           ,@delivery_option
           ,@option_category
)select @@IDENTITY as id
                    ";
             return  cn.QueryFirstOrDefault<string>(sql, _condition);
            }

        }
    }
}

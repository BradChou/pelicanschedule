﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Pelican.DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Pelican.DAL
{
    public class Pelican_request_DA : Base_DA
    {


        public Pelican_request_Condition GetPelican_requestByrequest_id(string pelican_check_number)
        {

            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[Pelican_request] WITH (NOLOCK)
                        WHERE pelican_check_number = @pelican_check_number
                    ";
                return cn.QueryFirstOrDefault<Pelican_request_Condition>(sql, new { pelican_check_number });
            }

        }

        public string GetttDeliveryScanLogLastScanItem(string checkNumber)
        {
            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[ttDeliveryScanLog] a WITH (NOLOCK)
                        INNER JOIN [dbo].[tcDeliveryRequests] b WITH (NOLOCK) 
                        on a.check_number=b.check_number
                        WHERE a.check_number = @checkNumber
                        and a.scan_item in (3)  
                        and a.arrive_option in ('3','8','49')
                    ";
                return cn.QueryFirstOrDefault<string>(sql, new { checkNumber });
            }
        }
    }
}

﻿using Dapper;
using Pelican.DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Pelican.DAL
{
    public class Pelican_return_content_DA : Base_DA
    {
        public Pelican_return_content_Condition GetPelican_requestByrequest_id(DateTime handy_time ,string pelican_check_number,string status_code)
        {

            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                        @"  SELECT *
                            FROM [dbo].[Pelican_return_content] WITH (NOLOCK)
                            WHERE pelican_check_number = @pelican_check_number AND status_code = @status_code AND handy_time = @handy_time
                    ";
                return cn.QueryFirstOrDefault<Pelican_return_content_Condition>(sql, new { pelican_check_number, status_code, handy_time });
            }

        }

        public void Insert(Pelican_return_content_Condition _condition)
        {

            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                    @" 
INSERT INTO [dbo].[Pelican_return_content]
           ([scan_log_id]
           ,[write_in_time]
           ,[return_filename]
           ,[account_code]
           ,[pelican_check_number]
           ,[fse_check_number]
           ,[receive_zip3]
           ,[information_create_date]
           ,[receive_date]
           ,[return_file_create_time]
           ,[end_code]
           ,[status_code]
           ,[scan_station_code]
           ,[scan_station_shortname]
           ,[handy_time]
           ,[status_chinese]
           ,[receive_code]
           ,[abnormal_status_code]
           ,[abnormal_status_chinese])
     VALUES
           (
		    @scan_log_id
           ,@write_in_time
           ,@return_filename
           ,@account_code
           ,@pelican_check_number
           ,@fse_check_number
           ,@receive_zip3
           ,@information_create_date
           ,@receive_date
           ,@return_file_create_time
           ,@end_code
           ,@status_code
           ,@scan_station_code
           ,@scan_station_shortname
           ,@handy_time
           ,@status_chinese
           ,@receive_code
           ,@abnormal_status_code
           ,@abnormal_status_chinese
		   )
                    ";
                cn.Execute(sql, _condition);
            }

        }
    }
}

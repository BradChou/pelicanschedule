﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.DAL
{
    public class Base_DA
    {
        static IConfiguration config = new ConfigurationBuilder() .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true) .Build();
        public readonly string AWSConnectionString = config.GetConnectionString("AWSConnectionString");
        public readonly string Authorization = config["Authorization"].ToString();
    }
}

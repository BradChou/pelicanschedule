﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Pelican.DAL.Model.Condition;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Pelican.DAL
{
    public class tcDeliveryRequests_DA : Base_DA
    {

        public tcDeliveryRequests_Condition GettcDeliveryRequestsByrequest_id(string check_number)
        {
            using (var cn = new SqlConnection(AWSConnectionString))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
                        WHERE check_number = @check_number
                    ";
                return cn.QueryFirstOrDefault<tcDeliveryRequests_Condition>(sql, new { check_number });
            }

        }

    }
}

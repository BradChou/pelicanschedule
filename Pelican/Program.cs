﻿using System;
using System.Net;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Imaging;

using System.Net.Mail;
using RestSharp;
using Pelican.BL;

namespace Pelican
{
    public class Program
    {
        // private static string LYName = @"捷川貨運何小姐";
        // private static string LYCity = @"桃園市";
        // private static string LYArea = @"大園區";
        // private static string LYAddress = @"國際路一段55號";          //已改
        // private static string LYTel = @"0984346261";

        // private static string AWSConnectionString = @"Initial Catalog=JunfuReal;server=172.30.1.34,1433; Initial Catalog = JunFuReal; Persist Security Info = True; User Id=lyAdmin;Password=P@ssw0rd!!!";
        // private static string TcDeliveryRequest = @"tcDeliveryRequests";
        // private static string toPelicanFormatDbTable = @"Pelican_post_format";
        // private static string requestPelican = @"Pelican_request";
        //// private static string logPelican = @"Pelican_log";              //單號資料的log
        // private static string logPelicanSchedule = @"Pelican_schedule_log";         //排程的log
        // private static string intermodalTransportationCheckNumber = @"intermodal_transportation_checknumber";
        // private static string pelicanStatusTable = @"Pelican_status";
        // private static int failTimes = 3; //一筆單號失敗嘗試次數
        // private static string JFCustomerCode = @"8362991401";         //正式客代
        // private static string pathSendToPelicanTxt = @"D:\PelicanFTPFile\";               //輸出需要上傳檔案路徑
        // private static string pathSendToPelicanTxtUploaded = @"D:\PelicanFTPFileUploaded\";//將已經上傳的檔案移至此
        // private static DateTime scheduleStartDate = new DateTime(2021, 2, 21);
        // private static int companyIdPelican = 1;//正式宅配通編號:1
        // private static string logPelicanFailInsertIntoPelicanRequest = @"D:\PelicanFailRequest\failRequests.txt";

        // private static string ftpUriUpload = @"ftp://p2ap3.e-can.com.tw/";          //
        // private static string ftpUriUploadReturn = @"Pickup/";      //逆物流跟正物流路徑不同
        // private static string ftpUriUploadRoundTripReturn = @"ReservedPickup/";     //來回件逆物流
        // private static string ftpAccountUpload = @"webedi";     //正式ftp帳號
        // private static string ftpPasswordUpload = @"0922430089";
        // private static string ftpUriDownload = @"ftp://p2ap3.e-can.com.tw/8362991401FY/";          //抓取回覆檔位置
        // private static string ftpAccountDownload = @"delivery";          //
        // private static string ftpPasswordDownload = @"0923505100";          //

        // private static string ftpReturnPath = @"D:\PelicanReturn\";           //宅配通回傳檔案路徑
        // private static string ftpFileReturnRecord = @"D:\PelicanReturnInserted\";                  //宅配通回傳檔案寫入完畢傳到這裡

        // private static string statusTable = @"Pelican_status";      //貨態對應表
        // private static string scanLogDriverCode = @"PP003";         //掃scan log的司機工號

        // private static string PelicanReturnContent = @"Pelican_return_content";     //回覆檔寫入table名字

        // private static string exceptionLogFolder = @"D:\PelicanExceptionCatcher\";
        // private static string exceptionLogFileName = @"exception.txt";

        // private static int alertIfCheckNumberCountLessThanTypeD = 2000;
        // private static int alertIfCheckNumberCountLessThanTypeR = 500;



        //private static bool IsPelicanCheckNumber(string checkNumber)        //這段看起來怪怪的，不過好像沒在用
        //{
        //    string cmdText = string.Format(@"select * from ");
        //    using (SqlCommand cmd = new SqlCommand())
        //    {
        //        cmd.Connection = new SqlConnection(AWSConnectionString);
        //        cmd.Connection.Open();

        //        cmd.Connection.Close();
        //    }
        //    return true;
        //}

        static void Main(string[] args)
        {
            //ReadReturnFile();
            PelicanBL _pelicanBL = new PelicanBL();


            DateTime latestTime = _pelicanBL.CatchLatestSuccessTime();           //抓取上次成功執行時間
            
            _pelicanBL.Catch99InDB(latestTime);                   //從資料庫抓取轉聯運的單號，並寫入專門table

            _pelicanBL.FtpUploaderToPelican();

            _pelicanBL.FtpCatchFromPelican();        //
            
            _pelicanBL.ReadReturnFile();           //讀取並寫入scanLog
            Console.WriteLine("執行完畢");

        }
    }




}

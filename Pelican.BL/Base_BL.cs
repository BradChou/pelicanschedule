﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pelican.BL
{
    public class Base_BL
    {
        static IConfiguration config = new ConfigurationBuilder()
      .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
      .Build();

        public readonly string AWSConnectionString = config.GetConnectionString("AWSConnectionString");
        public readonly string logPelicanSchedule = config["logPelicanSchedule"];
        public readonly string scheduleStartDate = config["scheduleStartDate"];
        public readonly string LYName = config["LYName"];
        public readonly string LYCity = config["LYCity"];
        public readonly string LYArea = config["LYArea"];
        public readonly string LYAddress = config["LYAddress"];
        public readonly string LYTel = config["LYTel"];
        public readonly string toPelicanFormatDbTable = config["toPelicanFormatDbTable"];
        public readonly int failTimes = int.Parse(config["failTimes"]);
        public readonly int alertIfCheckNumberCountLessThanTypeD = int.Parse(config["alertIfCheckNumberCountLessThanTypeD"]);
        public readonly int alertIfCheckNumberCountLessThanTypeR = int.Parse(config["alertIfCheckNumberCountLessThanTypeR"]);
        public readonly string TcDeliveryRequest = config["TcDeliveryRequest"];
        public readonly string requestPelican = config["requestPelican"];
        public readonly string intermodalTransportationCheckNumber = config["intermodalTransportationCheckNumber"];
        public readonly string companyIdPelican = config["companyIdPelican"];
        public readonly string logPelicanFailInsertIntoPelicanRequest = config["logPelicanFailInsertIntoPelicanRequest"];
        public readonly string ftpReturnPath = config["ftpReturnPath"];
        public readonly string pelicanStatusTable = config["pelicanStatusTable"];
        public readonly string JFCustomerCode = config["JFCustomerCode"];
        public readonly string scanLogDriverCode = config["scanLogDriverCode"];
        public readonly string PelicanReturnContent = config["PelicanReturnContent"];
        public readonly string ftpFileReturnRecord = config["ftpFileReturnRecord"];
        public readonly string pathSendToPelicanTxt = config["pathSendToPelicanTxt"];
        public readonly string ftpUriUpload = config["ftpUriUpload"];
        public readonly string ftpUriUploadReturn = config["ftpUriUploadReturn"];
        public readonly string ftpUriUploadRoundTripReturn = config["ftpUriUploadRoundTripReturn"];
        public readonly string pathSendToPelicanTxtUploaded = config["pathSendToPelicanTxtUploaded"];
        public readonly string ftpAccountUpload = config["ftpAccountUpload"];
        public readonly string ftpPasswordUpload = config["ftpPasswordUpload"];
        public readonly string ftpAccountDownload = config["ftpAccountDownload"];
        public readonly string ftpPasswordDownload = config["ftpPasswordDownload"];
        public readonly string ftpUriDownload = config["ftpUriDownload"];

        public readonly string exceptionLogFileName = config["exceptionLogFileName"];
        public readonly string exceptionLogFolder = config["exceptionLogFolder"];

       
    }
}

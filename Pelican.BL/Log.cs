﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Pelican.BL
{
    public class Log: Base_BL
    {

    
        public void ExceptionRecord(int step, Exception e)
        {

            if (!Directory.Exists(exceptionLogFolder))
            {
                Directory.CreateDirectory(exceptionLogFolder);
            }
            if (!File.Exists(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName))
            {
                var file = File.Create(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName);
                file.Close();
            }
            File.AppendAllText(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName, "Step: " + step + "   " + DateTime.Now.ToString() + "\r\n" + e.ToString() + "\r\n---------------------------------------------" + "\r\n");

            try
            {
                string cmdText = string.Format(@"update {0} set remark = '{1}' where id = {2}", logPelicanSchedule, e.ToString(), step);
                using (SqlCommand cmd = new SqlCommand(cmdText))
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }


        public void ExceptionRecordText(int step, string error)
        {

            if (!Directory.Exists(exceptionLogFolder))
            {
                Directory.CreateDirectory(exceptionLogFolder);
            }
            if (!File.Exists(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName))
            {
                var file = File.Create(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName);
                file.Close();
            }
            File.AppendAllText(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName, "Step: " + step + "   " + DateTime.Now.ToString() + "\r\n" + error + "\r\n---------------------------------------------" + "\r\n");

           
        }

        public int WriteDbLog(int step, bool isSuccess, bool isEnd, int request_id = -1)           //false:start true:end
        {

            string cmdText = "";
            if (!isEnd)
            {
                string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                cmdText = string.Format(@"insert into {0} (cdate, start_time, step) values ('{1}','{1}','{2}') select @@IDENTITY as id", logPelicanSchedule, now, step);
            }
            else
            {
                string boolToBit = isSuccess == true ? "1" : "0";
                string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                cmdText = string.Format(@"update {0} set is_success = {1}, end_time = '{2}' where id = {3} select @@IDENTITY as id", logPelicanSchedule, boolToBit, now, request_id);
            }

            int return_id = -1;
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (!isEnd)
                        {
                            while (dr.Read())
                            {
                                return_id = int.Parse(dr["id"].ToString());
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return return_id;
        }
    }
}

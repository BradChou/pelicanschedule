﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Pelican.DAL;
using Pelican.DAL.Model.Condition;
using Pelican.DAL.Model.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Pelican.BL
{
    public class PelicanBL : Base_BL
    {

        Log _log = new Log();
        Pelican_return_content_DA _pelican_return_content_DA = new Pelican_return_content_DA();
        ttDeliveryScanLog_DA _ttDeliveryScanLog_DA = new ttDeliveryScanLog_DA();
        public DateTime CatchLatestSuccessTime()
        {
            int insertId = _log.WriteDbLog(0, false, false);
            bool isSuccess = true;


            DateTime result = DateTime.Parse(scheduleStartDate);

            string cmdText = string.Format(@"select max(start_time)'latest_success_time' from {0} with(nolock) where is_success = 1 and step = 1", logPelicanSchedule);
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                cmd.Connection.Open();
                try
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr["latest_success_time"].ToString() != "")
                                result = DateTime.Parse(dr["latest_success_time"].ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(0, e);
                }
                cmd.Connection.Close();
            }
            _log.WriteDbLog(0, isSuccess, true, insertId);
            if (DateTime.Now.AddDays(-3) > result)          //避免為了一個錯誤單延誤整個程式
                result = DateTime.Now.AddDays(-3);

            return result;
        }

        public void Catch99InDB(DateTime latestSuccessTime)
        {
            int insertId = _log.WriteDbLog(1, false, false);
            bool isSuccess = true;
            string cmdFormatText = string.Format(@"select * from {0} with(nolock)", toPelicanFormatDbTable);
            List<PostFormatEntity> formatEntitiesList = new List<PostFormatEntity>();
            List<DeliveryRequestEntity> deliveryRequestEntities = new List<DeliveryRequestEntity>();
            using (SqlCommand cmd = new SqlCommand(cmdFormatText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            PostFormatEntity entity = new PostFormatEntity();
                            entity.Id = int.Parse(sr["Id"].ToString());
                            entity.IsNecessary = bool.Parse(sr["is_necessary"].ToString());
                            entity.ColumnName = sr["column_name"].ToString();
                            entity.IsNumberOnly = bool.Parse(sr["is_number_only"].ToString());
                            entity.StartLocation = int.Parse(sr["start_location"].ToString());
                            entity.EndLocation = int.Parse(sr["end_location"].ToString());
                            entity.Remark = sr["remark"].ToString();

                            formatEntitiesList.Add(entity);
                        }

                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(1, e);
                    Console.WriteLine(e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }


            //這段只抓正物流、來回件正物流
            string cmdText = string.Format(@"select * from tcDeliveryRequests d with(nolock) left join ttDeliveryRequestsRecord r with(nolock) on d.request_id = r.request_id where  d.print_date >= '{0}'
                                                and d.deliveryType='D' and d.print_date >= '{1}' and d.customer_code not like 'F2900210%' and d.check_number != '' and d.check_number not in (select jf_check_number from Pelican_request with(nolock))
                                                ", latestSuccessTime.AddDays(-30).ToString("yyyy-MM-dd"), scheduleStartDate);              //多抓一天防凌晨十二點，***記得拿掉測試字串
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = LYName,
                                SendTel = LYTel,
                                SendCity = LYCity,
                                SendArea = LYArea,
                                SendAddress = LYAddress,
                                ReceiveContact = sr["receive_contact"] == null ? string.Empty : StrTrimer(sr["receive_contact"].ToString()),
                                ReceiveTel = sr["receive_tel1"] == null ? string.Empty : StrTrimer(sr["receive_tel1"].ToString()),
                                ReceiveCity = sr["receive_city"] == null ? string.Empty : StrTrimer(sr["receive_city"].ToString()),
                                ReceiveArea = sr["receive_area"] == null ? string.Empty : StrTrimer(sr["receive_area"].ToString()),
                                ReceiveAddress = sr["receive_address"] == null ? string.Empty : StrTrimer(sr["receive_address"].ToString()),
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }






            //以下這段是逆物流，不含來回件逆物流
            string cmdTextReturn = string.Format(@"select * from tcDeliveryRequests d with(nolock) left join ttDeliveryRequestsRecord r with(nolock) on d.request_id = r.request_id where   d.print_date >= '{0}'
                                                and d.deliveryType='R' and (d.check_number like '990%' or d.check_number like '202%') and d.print_date >= '{1}' and d.customer_code not like 'F2900210%' and d.check_number != '' and d.round_trip = 0  and d.send_address not like '%299299419%' and d.customer_code <> 'F1300600002' and d.check_number not in (select jf_check_number from Pelican_request with(nolock))
                                                ", latestSuccessTime.AddDays(-1).ToString("yyyy-MM-dd"), scheduleStartDate);     //多抓一天防凌晨十二點，***記得拿掉測試字串

            using (SqlCommand cmd = new SqlCommand(cmdTextReturn))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.CommandTimeout = 2400;
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = sr["send_contact"].ToString(),
                                SendTel = sr["send_tel"].ToString(),
                                SendCity = sr["send_city"].ToString(),
                                SendArea = sr["send_area"].ToString(),
                                SendAddress = sr["send_address"].ToString(),
                                ReceiveContact = LYName,
                                ReceiveTel = LYTel,
                                ReceiveCity = LYCity,
                                ReceiveArea = LYArea,
                                ReceiveAddress = LYAddress,
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            //以下這段是來回件逆物流
            string cmdTextRoundTripReturn = string.Format(
 @"
SELECT d.request_id
 ,d.check_number
 ,d.order_number
 ,d.pieces
 ,d.send_contact
 ,d.send_tel
 ,d.send_city
 ,d.send_area
 ,d.send_address
 ,d.arrive_assign_date
 ,d.time_period
 ,d.collection_money
 ,d.DeliveryType
 ,d.invoice_desc
 ,d.round_trip
 ,d.roundtrip_checknumber
 ,d.cdate
FROM tcDeliveryRequests d WITH (NOLOCK)
LEFT JOIN ttDeliveryRequestsRecord r WITH (NOLOCK) ON d.request_id = r.request_id

WHERE  d.print_date >= '{0}'
 AND d.deliveryType = 'R'
 AND d.check_number LIKE '990%'
 AND d.print_date >= '{1}'
 AND d.send_contact NOT LIKE '%光年%'
 AND d.cancel_date is null
 AND d.check_number != ''
 AND round_trip = 1
 AND d.check_number NOT IN (
		SELECT jf_check_number
		FROM Pelican_request WITH (NOLOCK)
		)

", latestSuccessTime.AddDays(-1).ToString("yyyy-MM-dd"), scheduleStartDate
);     //多抓一天防凌晨十二點，***記得拿掉測試字串

            using (SqlCommand cmd = new SqlCommand(cmdTextRoundTripReturn))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.CommandTimeout = 2400;
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = sr["send_contact"].ToString(),
                                SendTel = sr["send_tel"].ToString(),
                                SendCity = sr["send_city"].ToString(),
                                SendArea = sr["send_area"].ToString(),
                                SendAddress = sr["send_address"].ToString(),
                                ReceiveContact = LYName,
                                ReceiveTel = LYTel,
                                ReceiveCity = LYCity,
                                ReceiveArea = LYArea,
                                ReceiveAddress = LYAddress,
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }


            List<PostZipEntity> zipList = new List<PostZipEntity>();
            string cmdZipText = @"select * from ttArriveSites with(nolock)";
            using (SqlCommand cmd = new SqlCommand(cmdZipText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            PostZipEntity pzEntity = new PostZipEntity()
                            {
                                PostArea = sr["post_area"].ToString(),
                                PostCity = sr["post_city"].ToString(),
                                Zip = sr["zip"].ToString()
                            };
                            zipList.Add(pzEntity);
                        }

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("取得3碼郵遞區號失敗");
                    isSuccess = false;
                    _log.ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            int total_fail = 0;
            int fail = 0;
            for (var i = 0; i < deliveryRequestEntities.Count; i++)
            {
                try
                {
                    PelicanCreateReuturnEntity apiReturnEntity = ApiRequestToPelican(deliveryRequestEntities[i].ReceiveCity + deliveryRequestEntities[i].ReceiveArea + deliveryRequestEntities[i].ReceiveAddress, deliveryRequestEntities[i].ReceiveZip3);
                    deliveryRequestEntities[i].IsNeedToInsertTable = apiReturnEntity.IsSuccess;

                    if (deliveryRequestEntities[i].IsNeedToInsertTable)     //若沒有取得宅配通郵遞區號則不寫入資料表
                    {
                        JfCustomerCodeAndPelicanCheckNumber temp = deliveryRequestEntities[i].DeliveryType == "D" ? GetPelicanCheckNumber(false, false, deliveryRequestEntities[i].JFCheckNumber) : GetPelicanCheckNumber(true, deliveryRequestEntities[i].RoundTrip.ToLower() == "true", deliveryRequestEntities[i].JFCheckNumber);
                        deliveryRequestEntities[i].PelicanCheckNumber = temp.PelicanCheckNumber==null?"":temp.PelicanCheckNumber.PadLeft(12, ' ');
                        deliveryRequestEntities[i].JfCustomerCode = temp.JfCustomerCode;
                        deliveryRequestEntities[i].SendZip3 = FromAddressToZip(deliveryRequestEntities[i].SendCity, deliveryRequestEntities[i].SendArea, zipList);
                        deliveryRequestEntities[i].ReceiveZip3 = FromAddressToZip(deliveryRequestEntities[i].ReceiveCity, deliveryRequestEntities[i].ReceiveArea, zipList);

                        deliveryRequestEntities[i].PelicanReceiveZip5 = apiReturnEntity.PZip5;
                        deliveryRequestEntities[i].PelicanReceiveArea = apiReturnEntity.Area;
                        deliveryRequestEntities[i].Remark = (deliveryRequestEntities[i].RoundTrip.ToLower() == "true" ? "回件:" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(0, 1) + "84" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(3, 9) : "") + deliveryRequestEntities[i].Remark;
                    }
                    fail = 0;
                }
                catch (Exception e)
                {
                    total_fail += 1;
                    fail += 1;
                    //onsole.WriteLine(deliveryRequestEntities[i].JFRequestId);
                    if (fail < failTimes)
                    {
                        i--;
                    }
                    _log.ExceptionRecord(1, e);
                    try
                    {
                        var deliveryRequest = deliveryRequestEntities[i];
                        string json = JsonConvert.SerializeObject(deliveryRequest);
                        _log.ExceptionRecordText(1, json);
                    }
                    catch { }
                }
                Console.WriteLine("處理API段中..." + i + @"/" + deliveryRequestEntities.Count + "，失敗" + total_fail + "次");
            }

            deliveryRequestEntities = deliveryRequestEntities.Where(d => !(d.PelicanCheckNumber is null) && d.IsNeedToInsertTable).ToList();      //避免超過貨號的資料也傳進去

            try
            {
                Alertor(deliveryRequestEntities.Where(x => x.DeliveryType == "D").Count(), deliveryRequestEntities.Where(x => x.DeliveryType == "R").Count());      //警示器
            }
            catch (Exception ex)
            {

            }
            TraslateAndExportToTxt(deliveryRequestEntities, formatEntitiesList);

            for (var i = 0; i < deliveryRequestEntities.Count; i++)
            {
                //deliveryRequestEntities[i] = CheckDeliveryRequestContainsSQLIllegle(deliveryRequestEntities[i]);      //這段已不需要，防止客戶輸入特殊字元，但有BUG仙註解掉
                if (!InsertRequestNewRequests(deliveryRequestEntities[i], insertId.ToString()))
                {
                    isSuccess = false;
                };
                Console.WriteLine("已嵌入Pelican_request" + (i + 1) + @"/" + deliveryRequestEntities.Count);
            }


            _log.WriteDbLog(1, isSuccess, true, insertId);


        }

        public void Alertor(int thisTimeNeedCheckNumberCountTypeD, int thisTimeNeedCheckNumberCountTypeR)
        {
            bool isSuccess = true;
            int insertId = _log.WriteDbLog(7, false, false);
            ResidueCheckNumber ResidueTypeD = ResidueCheckNumberCountChecker("D");
            ResidueCheckNumber ResidueTypeR = ResidueCheckNumberCountChecker("R");
            int countResidueTypeD = ResidueTypeD.ResidueCheckNumberCount;
            int countResidueTypeR = ResidueTypeR.ResidueCheckNumberCount;

            if (countResidueTypeD != -1 && countResidueTypeD - thisTimeNeedCheckNumberCountTypeD < alertIfCheckNumberCountLessThanTypeD)
            {
                if (SendMail(true, countResidueTypeD))
                {
                    UpdateAlertTime(ResidueTypeD.Ids);
                };
            }

            if (countResidueTypeR != -1 && countResidueTypeR - thisTimeNeedCheckNumberCountTypeR < alertIfCheckNumberCountLessThanTypeR)
            {
                if (SendMail(false, countResidueTypeR))
                {
                    UpdateAlertTime(ResidueTypeR.Ids);
                };
            }

            _log.WriteDbLog(7, isSuccess, true, insertId);
        }
        public bool SendMail(bool isDelivery, int residueCheckNumberCount)       //只允許gmail寄信
        {
            string sendAccount = "";
            string sendPassword = "";
            List<string> receiveAccounts = new List<string>();
            DateTime now = DateTime.Now;

            string cmdTextForSender = string.Format("select TOP 1 * from PelicanEmail with(nolock) where is_sender = 1 and active_date < '{0}'  and '{0}' < unable_date and mail_password is not null", now.ToString("yyyy-MM-dd HH:mm:ss"));
            using (SqlCommand cmdChooseSender = new SqlCommand(cmdTextForSender))
            {
                try
                {
                    using (cmdChooseSender.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseSender.Connection.Open();
                        using (SqlDataReader sr = cmdChooseSender.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                sendAccount = sr["mail_account"].ToString();
                                sendPassword = sr["mail_password"].ToString();
                            }
                        };

                    };
                }
                catch (Exception ex)
                {
                    _log.ExceptionRecord(999, ex);
                    return false;
                }
                finally
                {
                    cmdChooseSender.Connection.Close();
                }
            }

            string cmdTextForReceiver = string.Format("select * from PelicanEmail with(nolock) where is_sender = 0 and active_date < '{0}' and '{0}' < unable_date", now.ToString("yyyy-MM-dd HH:mm:ss"));
            using (SqlCommand cmdChooseReceivers = new SqlCommand(cmdTextForReceiver))
            {
                try
                {
                    using (cmdChooseReceivers.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseReceivers.Connection.Open();
                        using (SqlDataReader sr = cmdChooseReceivers.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                receiveAccounts.Add(sr["mail_account"].ToString());
                            }
                        };
                        cmdChooseReceivers.Connection.Close();
                    };
                }
                catch (Exception ex)
                {
                    _log.ExceptionRecord(999, ex);
                    return false;
                }
                finally
                {
                    cmdChooseReceivers.Connection.Close();
                }
            }


            try
            {
                if (receiveAccounts.Count() > 0)
                {
                    string smtpAddress = "smtp.gmail.com";
                    //設定Port
                    int portNumber = 587;
                    bool enableSSL = true;
                    //填入寄送方email和密碼
                    string emailFrom = sendAccount;
                    string password = sendPassword;
                    //主旨
                    string subject = string.Format("宅配通{0}物流貨號剩餘不足{1}組", isDelivery ? "正" : "逆", isDelivery ? alertIfCheckNumberCountLessThanTypeD.ToString() : alertIfCheckNumberCountLessThanTypeR.ToString());
                    //內容
                    string body = string.Format("{0} 宅配通{1}物流貨號剩餘{2}組", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), isDelivery ? "正" : "逆", residueCheckNumberCount.ToString());

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        for (var r = 0; r < receiveAccounts.Count(); r++)
                        {
                            mail.To.Add(receiveAccounts[r]);
                        }
                        mail.Subject = subject;
                        mail.Body = body;
                        // 若你的內容是HTML格式，則為True
                        mail.IsBodyHtml = false;

                        using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                        {
                            smtp.Credentials = new NetworkCredential(emailFrom, password);
                            smtp.EnableSsl = enableSSL;
                            smtp.Send(mail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.ExceptionRecord(999, ex);
                return false;
            }

            return true;
        }

        public void UpdateAlertTime(string ids)
        {
            string cmdText = string.Format(@"update {0} set alert_not_enough_time = '{1}' where id in ({2})", intermodalTransportationCheckNumber, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ids);
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    using (cmd.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmd.Connection.Open();
                        cmd.ExecuteReader();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }
        public ResidueCheckNumber ResidueCheckNumberCountChecker(string DorR)
        {

            string cmdTextForSender = string.Format("select * from {0} with(nolock) where active_flag = 1 and delivery_type = '{2}' and company_id = {1} and priority in (select priority from {0} where active_flag = 1 and delivery_type = '{2}' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican, DorR);
            int count = 0;
            bool isNeedToAlert = false;
            string ids = "";

            using (SqlCommand cmdChooseSender = new SqlCommand(cmdTextForSender))
            {
                try
                {
                    using (cmdChooseSender.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseSender.Connection.Open();
                        using (SqlDataReader sr = cmdChooseSender.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                count += (int)((long.Parse(sr["number_end"].ToString()) - long.Parse(sr["number_next"].ToString())) / 10);
                                isNeedToAlert = isNeedToAlert || sr["alert_not_enough_time"].ToString() == "";
                                ids += sr["id"].ToString() + ",";
                            }
                        };

                    };
                }
                catch (Exception ex)
                {
                    _log.ExceptionRecord(999, ex);
                }
                finally
                {
                    cmdChooseSender.Connection.Close();
                }
            }

            ResidueCheckNumber result = new ResidueCheckNumber()
            {
                Ids = ids.Substring(0, ids.Length - 1),
                ResidueCheckNumberCount = isNeedToAlert ? count : -1
            };

            return result;
        }
        public static string DefineMorningOrAfternoon(string input)
        {
            if (input.Contains("早"))
            {
                return "01";
            }
            else if (input.Contains("午") || input.Contains("晚"))
            {
                return "02";
            }
            else                    //不指定
            {
                return "00";
            }
        }
        public static PelicanCreateReuturnEntity ApiRequestToPelican(string address, string zip3)           //代入三碼郵遞區號，用在API無法辨識時使用
        {
            PelicanCreateReuturnEntity result = new PelicanCreateReuturnEntity();
            //string patternString = "\"PZip5\":\"";
            //WebRequest wr = WebRequest.Create(@"http://query1.e-can.com.tw:8080/datasnap/rest/tservermt/lookupzip/" + address);
            //HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
            //Stream dataStream = response.GetResponseStream();
            //StreamReader reader = new StreamReader(dataStream);
            //string responseFromServer = reader.ReadToEnd().Replace("{\"result\":[", "").Replace("]}", "");
            ///*int indexForResult = responseFromServer.IndexOf(patternString);           //用Jsonconvert代替
            //result = responseFromServer.Substring(indexForResult + 9, 5);*/

            string apiURL = @"http://query1.e-can.com.tw:8080/datasnap/rest/tservermt/lookupzip/";

            var client = new RestClient(apiURL);

            //   var request = new RestRequest(address);

            var request = new RestRequest("{address}", Method.GET);
            request.AddParameter("address", address, ParameterType.UrlSegment);//處理可能出現的保留字

            var response = client.Get<object>(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var json = JsonConvert.DeserializeObject<lookupzip>(response.Content);

                var rr = json.result.FirstOrDefault();
                if (rr == null)
                {

                    result.IsSuccess = false;
                    return result;
                    //result.PZip5 = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).PZip5;
                    //result.Area = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).Area;
                    //reader.Close();
                    //dataStream.Close();
                    //response.Close();

                }
                else
                {
                    result.PZip5 = rr.PZip5;
                    result.Area = rr.Area;

                    int temp;
                    if (result.PZip5.Length == 5 && int.TryParse(result.PZip5, out temp))
                    {
                        result.IsSuccess = true;
                        return result;
                    }
                    else
                    {
                        //result.PZip5 = zip3 + "00";
                        result.IsSuccess = false;
                        return result;
                    }
                }
            }
            else
            {
                result.IsSuccess = false;
                return result;
            }
        }
        public JfCustomerCodeAndPelicanCheckNumber GetPelicanCheckNumber(bool isReturnDelivery, bool isRoundTripReturn, string fseDeliveryCheckNumber)      //false:正物流 true:逆物流/true:來回件的"回件"
        {
            JfCustomerCodeAndPelicanCheckNumber result = new JfCustomerCodeAndPelicanCheckNumber();
            bool isExceed = false;      //是否超過結束單號
            string cmdText = @"";
            if (isRoundTripReturn)      //來回件的回件比較特別，不取單號，從其宅配通正物流單號2、3碼改成"84"
            {
                cmdText = string.Format(@"select SUBSTRING(pelican_check_number, 1, 1) + '84' + SUBSTRING(pelican_check_number, 4, 9)'number_next', i.customer_code, i.number_end from {0} d with(nolock) left join {1} p with(nolock) on d.check_number = p.roundtrip_checknumber left join {2} i with(nolock) on p.jf_customer_code = i.customer_code where i.delivery_type = 'R' and active_flag = 1 and company_id = {3}
                and check_number = '{4}'", TcDeliveryRequest, requestPelican, intermodalTransportationCheckNumber, companyIdPelican, fseDeliveryCheckNumber);
            }
            else if (!isReturnDelivery)
            {
                cmdText = string.Format(@"select * from {0} with(nolock) where active_flag = 1 and delivery_type = 'D' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'D' and company_id = {1} and number_end>=number_next)
                            update {0} set number_next = convert(varchar, convert(bigint,SUBSTRING(number_next,0,12)) + 1) + convert(varchar,(convert(bigint,SUBSTRING(number_next,0,12))+1)%7) where active_flag = 1 and delivery_type = 'D' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'D' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican);
            }
            else
            {
                cmdText = string.Format(@"select * from {0} with(nolock) where active_flag = 1 and delivery_type = 'R' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'R' and company_id = {1} and number_end>=number_next)
                            update {0} set number_next = convert(varchar, convert(bigint,SUBSTRING(number_next,0,12)) + 1) + convert(varchar,(convert(bigint,SUBSTRING(number_next,0,12))+1)%7) where active_flag = 1 and delivery_type = 'R' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'R' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican);
            }
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);

                    cmd.Connection.Open();
                    int records = 0;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.PelicanCheckNumber = dr["number_next"].ToString();
                            result.JfCustomerCode = dr["customer_code"].ToString();
                            result.NumberEnd = dr["number_end"].ToString();
                            records++;
                        }
                        if (records == 0)
                        {
                            isExceed = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    _log.ExceptionRecord(999, e);

                    try
                    {
                        string json = JsonConvert.SerializeObject(new { isReturnDelivery, isRoundTripReturn, fseDeliveryCheckNumber });
                        _log.ExceptionRecordText(999, json + "\r\n" + cmdText);
                    }
                    catch { }

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            if (isExceed)
            {
                try
                {
                    _log.WriteDbLog(999, false, false);       //超過貨號區間了
                }
                catch
                {

                }
            }
            return result;
        }
        public static string FromAddressToZip(string City, string Area, List<PostZipEntity> zipList)
        {
            string zip = zipList.Where(x => x.PostCity.Equals(City) && x.PostArea.Equals(Area)).ToArray().Length != 0 ? zipList.Where(x => x.PostCity.Equals(City) && x.PostArea.Equals(Area)).FirstOrDefault().Zip : "";
            return zip;
        }



        public void TraslateAndExportToTxt(List<DeliveryRequestEntity> inputList, List<PostFormatEntity> formatEntities)
        {
            //先檢查是否符合格式
            int id = _log.WriteDbLog(2, false, false);
            bool isSuccess = true;
            for (var i = 0; i < inputList.Count; i++)
            {
                inputList[i].Pieces = LengthTrimer(inputList[i].Pieces, 3);
                inputList[i].OrderNumber = LengthTrimer(inputList[i].OrderNumber, 32);
                inputList[i].SendContact = LengthTrimer(((inputList[i].RoundTrip.ToLower() == "true" && inputList[i].DeliveryType == "R") ? "[來回]" : "") + inputList[i].SendContact, 32);
                inputList[i].SendTel = LengthTrimer(inputList[i].SendTel, 32);
                inputList[i].ReceiveContact = LengthTrimer(((inputList[i].RoundTrip.ToLower() == "true" && inputList[i].DeliveryType == "D") ? "[來回]" : "") + inputList[i].ReceiveContact, 32);
                inputList[i].ReceiveTel = LengthTrimer(inputList[i].ReceiveTel, 32);
                inputList[i].CollectionMoney = LengthTrimer(inputList[i].CollectionMoney, 8);
                inputList[i].Remark = LengthTrimer(inputList[i].Remark, 60);
                if (inputList[i].SendCity.Length + inputList[i].SendArea.Length + inputList[i].SendAddress.Length > 64)
                {
                    int addressResidueLength = 64 - inputList[i].SendCity.Length - inputList[i].SendArea.Length;
                    inputList[i].SendAddress = LengthTrimer(inputList[i].SendAddress, addressResidueLength);
                }
                if (inputList[i].ReceiveCity.Length + inputList[i].ReceiveArea.Length + inputList[i].ReceiveAddress.Length > 64)
                {
                    int addressResidueLength = 64 - inputList[i].ReceiveCity.Length - inputList[i].ReceiveArea.Length;
                    inputList[i].ReceiveAddress = LengthTrimer(inputList[i].ReceiveAddress, addressResidueLength);
                }

            }

            string resultD = "";
            string resultR = "";
            string resultP_D = "";
            string resultP_R = "";
            int no_D = 0; //序號
            int no_R = 0;   //逆物流序號
            int no_P_D = 0; //來回件
            int no_P_R = 0;
            //List<string> resultList = new List<string>();
            //List<string> resultListReturn = new List<string>();
            for (var i = 0; i < inputList.Count; i++)
            {
                string result = "";
                if (inputList[i].RoundTrip.ToLower() != "true")      //是否為來回件
                {
                    if (inputList[i].DeliveryType == "R")           //正逆物流分開計
                    {
                        no_R += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_R.ToString(), 4, true);
                    }
                    else
                    {
                        no_D += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_D.ToString(), 4, true);
                    }
                }
                else //if (inputList[i].RoundTripCheckNumber != "")     //因為DeliveryRequests的逆物流roundtrip_checknumber沒有處理好，所以先暫時註解掉
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        no_P_R += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_P_R.ToString(), 4, true);
                    }
                    else
                    {
                        no_P_D += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_P_D.ToString(), 4, true);
                    }
                }

                result += TransUTF8ToBig5ByteAndTrimLength(DateTime.Now.ToString("yyyyMMdd"), 8, false);
                for (var j = 0; j < 8; j++)
                {
                    result += " ";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].PelicanCheckNumber, 12, false);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].Pieces.ToString(), 3, true);
                result += "01";             //設定S60
                result += inputList[i].JfCustomerCode;               //客戶代號
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].JFCheckNumber.ToString(), 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendContact, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendTel, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendZip3, 3, false);
                result += TransUTF8ToBig5ByteAndTrimLength((inputList[i].SendCity + inputList[i].SendArea + inputList[i].SendAddress), 64, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveContact, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveTel, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveZip3, 3, false);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveCity + inputList[i].ReceiveArea + inputList[i].ReceiveAddress, 64, false, true);
                for (var j = 0; j < 30; j++)
                {
                    result += " ";
                }
                for (var j = 0; j < 10; j++)
                {
                    result += " ";
                }
                for (var j = 0; j < 8; j++)
                {
                    result += " ";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].AssignedTimePeriod, 2, false);
                for (var j = 0; j < 1; j++)
                {
                    result += "1";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].CollectionMoney.ToString(), 8, true);
                result += "01";      //商品別，01一般
                result += inputList[i].PelicanReceiveZip5;              //收件地址分貨碼
                for (var j = 0; j < 1; j++)
                {
                    result += " ";
                }
                result += inputList[i].DeliveryType.ToString() == "R" ? "2" : "1";
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].Remark, 60, false, true);
                result += inputList[i].CollectionMoney == "0" ? "0" : "1";
                for (var j = 0; j < 42; j++)
                {
                    result += " ";
                }
                result = TransUTF8ToBig5ByteAndTrimLength(result, 512, false);
                result += "\r\n";

                if (inputList[i].RoundTrip.ToLower() != "true")
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        //resultListReturn.Add(result);
                        resultR += result;
                    }
                    else
                    {
                        //resultList.Add(result);
                        resultD += result;
                    }
                }
                else
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        //resultListReturn.Add(result);
                        resultP_R += result;
                    }
                    else
                    {
                        //resultList.Add(result);
                        resultP_D += result;
                    }
                }

                int textLength = result.Length;                     //判定長度是否正確
                Console.WriteLine("本筆字串長度:" + textLength);
            }

            isSuccess = CreateFtpFile(resultD, id, 0) && CreateFtpFile(resultR, id, 1) && CreateFtpFile(resultP_D, id, 2) && CreateFtpFile(resultP_R, id, 3);


            _log.WriteDbLog(2, isSuccess, true, id);
        }

        private static string LengthTrimer(string input, int length)
        {
            input = input.Replace("\r\n", "").Replace("\n", "").Replace("\t", "").Replace("\r", "");
            string result = input;
            if (input.Length > length)
            {
                result = input.Substring(0, length);
            }
            return result;
        }

        private static string StrTrimer(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (str.Length == 0)
            {
                return string.Empty;
            }

            return str.Replace("\r\n", "").Replace("\n", "").Replace("\t", "").Replace("\r", "").Trim();
        }

        private static string SqlStringRegular(string str)           //除錯
        {
            if (str.Length > 0)
            {
                str = str.Replace("'", "''");
            }

            return str;
        }


        public bool InsertRequestNewRequests(DeliveryRequestEntity entity, string logId)
        {
            bool isSuccess = true;

            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string failTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string cmdText = "";
            if (entity.ArriveAssignDate == "")
            {
                cmdText = string.Format(@"insert into {0} (log_id, pelican_check_number,jf_request_id,jf_check_number,delivery_type,cdate,udate,cuser,uuser,fail_times,fail_log_id,fail_at,order_number,pieces,send_contact,send_tel,send_city,send_area,send_address,receive_contact,receive_tel,receive_city,receive_area,receive_address,assigned_time_period,collection_money,remark,jf_customer_code,jf_send_zip3,jf_receive_zip3,pelican_receive_zip5,pelican_receive_area,round_trip,dr_request_id,roundtrip_checknumber) values ({1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12},'{13}',{14},'{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}',{26},'{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}')", requestPelican, logId, entity.PelicanCheckNumber, entity.JFRequestId, entity.JFCheckNumber, entity.DeliveryType, now, now, "schedule", "schedule", 0, -1, -1, SqlStringRegular(entity.OrderNumber), entity.Pieces, SqlStringRegular(entity.SendContact), SqlStringRegular(entity.SendTel), entity.SendCity, entity.SendArea, SqlStringRegular(entity.SendAddress), SqlStringRegular(entity.ReceiveContact), SqlStringRegular(entity.ReceiveTel), entity.ReceiveCity, entity.ReceiveArea, SqlStringRegular(entity.ReceiveAddress), entity.AssignedTimePeriod, entity.CollectionMoney, SqlStringRegular(entity.Remark), entity.JfCustomerCode, entity.SendZip3, entity.ReceiveZip3, entity.PelicanReceiveZip5, entity.PelicanReceiveArea, entity.RoundTrip, entity.DrRequestId, entity.RoundTripCheckNumber);
            }
            else
            {
                cmdText = string.Format(@"insert into {0} (log_id, pelican_check_number,jf_request_id,jf_check_number,delivery_type,cdate,udate,cuser,uuser,fail_times,fail_log_id,fail_at,order_number,pieces,send_contact,send_tel,send_city,send_area,send_address,receive_contact,receive_tel,receive_city,receive_area,receive_address,assigned_time_period,collection_money,remark,jf_customer_code,jf_send_zip3,jf_receive_zip3,pelican_receive_zip5,pelican_receive_area,round_trip,dr_request_id,roundtrip_checknumber,arrive_assign_date) values ({1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12},'{13}',{14},'{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}',{26},'{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}')", requestPelican, logId, entity.PelicanCheckNumber, entity.JFRequestId, entity.JFCheckNumber, entity.DeliveryType, now, now, "schedule", "schedule", 0, -1, -1, SqlStringRegular(entity.OrderNumber), entity.Pieces, SqlStringRegular(entity.SendContact), SqlStringRegular(entity.SendTel), entity.SendCity, entity.SendArea, SqlStringRegular(entity.SendAddress), SqlStringRegular(entity.ReceiveContact), SqlStringRegular(entity.ReceiveTel), entity.ReceiveCity, entity.ReceiveArea, SqlStringRegular(entity.ReceiveAddress), entity.AssignedTimePeriod, entity.CollectionMoney, SqlStringRegular(entity.Remark), entity.JfCustomerCode, entity.SendZip3, entity.ReceiveZip3, entity.PelicanReceiveZip5, entity.PelicanReceiveArea, entity.RoundTrip, entity.DrRequestId, entity.RoundTripCheckNumber, entity.ArriveAssignDate);
            }
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(entity.JFRequestId + e.ToString());

                    try
                    {
                        if (!File.Exists(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt"))
                        {
                            var newFile = File.Create(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt");
                            newFile.Close();
                        }
                        string[] failDetail = new string[]
                        {
                        cmdText,
                        "----------------------------------------------------------------"
                        };

                        File.AppendAllLines(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt", failDetail);
                    }
                    catch
                    {

                    }
                    isSuccess = false;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            return isSuccess;
        }


        private static string GetBarcodeImage(string input)     //沒在用
        {
            string SavePath = @"E:\barcode.png";
            //Barcode条码需在前后加上*字号代表开始与结束
            Bitmap barcode = CreateBarCode("*" + input + "*");
            barcode.Save(SavePath, ImageFormat.Png);
            barcode.Dispose();

            //将图片文件转成Base64字符串
            using (var fs = new FileStream(SavePath, FileMode.Open, FileAccess.Read))
            {
                var buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                string base64String = Convert.ToBase64String(buffer);
                string ImgBase64 = string.Format("data:image/png;base64,{0}'", base64String);
                return ImgBase64;
            }



        }

        public static Bitmap CreateBarCode(string data)
        {
            Bitmap barcode = new Bitmap(1, 1);
            Font threeOfNine = new Font("IDAutomationHC39M", 60,
                                    System.Drawing.FontStyle.Regular,
                                    System.Drawing.GraphicsUnit.Point);

            Graphics graphics = Graphics.FromImage(barcode);

            SizeF dataSize = graphics.MeasureString(data, threeOfNine);

            barcode = new Bitmap(barcode, dataSize.ToSize());
            graphics = Graphics.FromImage(barcode);
            graphics.Clear(Color.White);

            graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;

            graphics.DrawString(data, threeOfNine, new SolidBrush(Color.Black), 0, 0);

            graphics.Flush();
            threeOfNine.Dispose();
            graphics.Dispose();

            return barcode;

        }

        public static string TransUTF8ToBig5ByteAndTrimLength(string content, int lengthLimit, bool isFillWithZero, bool isFillFromRight = false)        //emptyOrZero 不足長度補零或是空白 empty:空白 true:補零，isFillFromRight false:從左邊開始補 true:從右邊開始補
        {
            int lengthCalculatorByte = 0;
            byte[] byteResult = new byte[0];
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            for (var i = 0; i < content.Length; i++)
            {
                string temp = content.Substring(i, 1);
                byte[] tempByte = Encoding.GetEncoding("big5").GetBytes(temp);
                lengthCalculatorByte += tempByte.Length;
                if (lengthCalculatorByte > lengthLimit)
                {
                    break;
                }
                byteResult = byteResult.Concat(tempByte).ToArray();
            }
            int nowByteLength = byteResult.Length;
            for (var j = 0; j < lengthLimit - nowByteLength; j++)
            {
                if (isFillWithZero)
                {
                    if (isFillFromRight)
                    {
                        byteResult = byteResult.Concat(Encoding.GetEncoding("big5").GetBytes("0")).ToArray();
                    }
                    else
                    {
                        byteResult = Encoding.GetEncoding("big5").GetBytes("0").Concat(byteResult).ToArray();
                    }
                }
                else
                {
                    if (isFillFromRight)
                    {
                        byteResult = byteResult.Concat(Encoding.GetEncoding("big5").GetBytes(" ")).ToArray();
                    }
                    else
                    {
                        byteResult = Encoding.GetEncoding("big5").GetBytes(" ").Concat(byteResult).ToArray();
                    }
                }
            }
            string result = Encoding.GetEncoding("big5").GetString(byteResult);

            return result;
        }


        private List<PelicanStatusEntity> GetPelicanStatusEntity()
        {
            List<PelicanStatusEntity> statusEntities = new List<PelicanStatusEntity>();

            string cmdStatusText = string.Format(@"select * from {0} with(nolock)", pelicanStatusTable);
            using (SqlCommand cmd = new SqlCommand(cmdStatusText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                cmd.Connection.Open();
                try
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PelicanStatusEntity status = new PelicanStatusEntity()
                            {
                                ArriveOption = reader["arrive_option"].ToString(),
                                ScanItem = reader["scan_item"].ToString(),
                                PelicanStatusCode = reader["pelican_status_code"].ToString(),
                                IsDeliveryD = reader["is_delivery_D"].ToString().ToLower() == "true" ? true : false,
                                IsDeliveryR = reader["is_delivery_R"].ToString().ToLower() == "true" ? true : false,
                                EndCode = reader["pelican_end_code"].ToString()
                            };
                            statusEntities.Add(status);
                        }
                    }
                }
                catch (Exception e)
                {
                    statusEntities = null;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            return statusEntities;
        }

        private List<TtDeliveryScanLogEntity> ParsingPelicanScanLog(FileInfo f, List<PelicanStatusEntity> statusEntities)
        {
            List<TtDeliveryScanLogEntity> thisTimeInsertIntoScanLog = new List<TtDeliveryScanLogEntity>();


            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);                      //這裡讀取回覆檔內容
            bool isDFile = !f.Name.Contains("R" + JFCustomerCode);//是否為正物流
            using (StreamReader sourceStream = new StreamReader(f.FullName, Encoding.GetEncoding("big5")))
            {
                byte[] fileContent = Encoding.GetEncoding("big5").GetBytes(sourceStream.ReadToEnd());
                int records = fileContent.Length / 578;                 //每筆長度578
                for (var i = 0; i < records; i++)
                {
                    byte[] thisRecord = new byte[576];
                    Array.Copy(fileContent, i * 578, thisRecord, 0, 576);       //加上換行符號長度

                    string endCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 506, 1)).Trim(' ');
                    string statusCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 507, 4)).Trim(' ');
                    string statusCodeDetail = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 542, 5)).Trim(' ');
                    string scanDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 518, 14)).Trim(' ');
                    try
                    {
                        ScanItemAndArriveOptionEntity sa = ReturnSA(endCode, statusCode, statusCodeDetail, statusEntities, isDFile);

                        TtDeliveryScanLogEntity scanLog = new TtDeliveryScanLogEntity()
                        {
                            AreaArriveCode = "99",
                            ArriveOption = sa.ArriveOption,
                            CDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                            CheckNumber = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 25, 32)).Trim(' '),
                            DriverCode = scanLogDriverCode,
                            ScanDate = scanDate.Insert(12, ":").Insert(10, ":").Insert(8, " ").Insert(6, "-").Insert(4, "-"),
                            ScanItem = sa.ScanItem,
                            ScanLogId = "",
                            ReturnFilename = f.Name,
                            AccountCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 0, 10)),
                            PelicanCheckNumber = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 10, 12)),
                            ReceiveZip3 = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 254, 3)),
                            InformationCreateDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 341, 8)).Insert(6, "-").Insert(4, "-"),
                            ReceiveDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 349, 8)).Trim(' ').Length > 0 ? Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 349, 8)).Insert(6, "-").Insert(4, "-") : "",
                            ReturnFileCreateTime = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 492, 14)).Insert(12, ":").Insert(10, ":").Insert(8, " ").Insert(6, "-").Insert(4, "-"),
                            EndCode = endCode,
                            StatusCode = statusCode,
                            ScanStationCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 511, 3)),
                            ScanStationShortname = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 514, 4)),
                            StatusChinese = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 532, 8)),
                            ReceiveCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 540, 2)),
                            AbnormalStatusCode = statusCodeDetail,
                            AbnormalStatusChinese = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 547, 12)),
                            IsD = isDFile
                        };
                        thisTimeInsertIntoScanLog.Add(scanLog);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }


            return thisTimeInsertIntoScanLog;
        }

        private List<TtDeliveryScanLogEntity> CheckPelicanCheckNumber(List<TtDeliveryScanLogEntity> thisTimeInsertIntoScanLog)
        {
            string exceptionLogFolderFile = exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + "CheckNumber.txt";

            if (!Directory.Exists(exceptionLogFolder))
            {
                Directory.CreateDirectory(exceptionLogFolder);
            }
            if (!File.Exists(exceptionLogFolderFile))
            {
                var file = File.Create(exceptionLogFolderFile);
                file.Close();
            }

            try
            {
                foreach (var item in thisTimeInsertIntoScanLog)
                {
                    //如果P 沒有給我們傳回來CheckNumber 我們自己回去找
                    if (string.IsNullOrEmpty(item.CheckNumber))
                    {
                        Pelican_request_DA _Pelican_request_DA = new Pelican_request_DA();
                        tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                        var Pelican_requestInfo = _Pelican_request_DA.GetPelican_requestByrequest_id(item.PelicanCheckNumber);
                        //如果是空就不管了
                        if (Pelican_requestInfo != null)
                        {
                            //以防萬一 主表再查一次
                            if (!string.IsNullOrEmpty(Pelican_requestInfo.jf_check_number))
                            {
                                File.AppendAllText(exceptionLogFolderFile, "Pelican_requestInfo jf_check_number: " + Pelican_requestInfo.jf_check_number + "   " + DateTime.Now.ToString() + "\r\n");
                                var tcDeliveryRequestsInfo = _tcDeliveryRequests_DA.GettcDeliveryRequestsByrequest_id(Pelican_requestInfo.jf_check_number);

                                if (tcDeliveryRequestsInfo != null)
                                {
                                    File.AppendAllText(exceptionLogFolderFile, "tcDeliveryRequestsInfo check_number: " + tcDeliveryRequestsInfo.check_number + "   " + DateTime.Now.ToString() + "\r\n");
                                    item.CheckNumber = tcDeliveryRequestsInfo.check_number;
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                }



            }
            catch (Exception e)
            {
                File.AppendAllText(exceptionLogFolderFile, "Exception Message: " + e.Message + "   " + DateTime.Now.ToString() + "\r\n");
                File.AppendAllText(exceptionLogFolderFile, "Exception StackTrace: " + e.StackTrace + "   " + DateTime.Now.ToString() + "\r\n");
                File.AppendAllText(exceptionLogFolderFile, "Exception Source: " + e.Source + "   " + DateTime.Now.ToString() + "\r\n");
                return null;
            }



            return thisTimeInsertIntoScanLog;
        }
        Notification_DAL _Notification_DAL = new Notification_DAL();

        public void ReadReturnFile()
        {

            int insertId = _log.WriteDbLog(3, false, false);
            int logInsertId = _log.WriteDbLog(5, false, false);

            string exceptionLogFolderFile = exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + "ReturnEx.txt";

            if (!Directory.Exists(exceptionLogFolder))
            {
                Directory.CreateDirectory(exceptionLogFolder);
            }
            if (!File.Exists(exceptionLogFolderFile))
            {
                var file = File.Create(exceptionLogFolderFile);
                file.Close();
            }


            bool isSuccess = true;
            bool isLogSuccess = true;

            //取得所有檔案
            DirectoryInfo dirs = new DirectoryInfo(ftpReturnPath);
            IEnumerable<FileInfo> files = dirs.GetFiles("*.txt");

            //取得宅配通狀態

            var statusEntities = GetPelicanStatusEntity();


            //一個一個檔案處理

            foreach (var file in files)
            {
                try
                {
                    //解析
                    var PelicanScanInfo = ParsingPelicanScanLog(file, statusEntities);

                    if (PelicanScanInfo == null)
                    {
                        throw new Exception("解析失敗");
                    }
                    //整理
                    //可能沒有回傳checknumber
                    PelicanScanInfo = CheckPelicanCheckNumber(PelicanScanInfo);
                    if (PelicanScanInfo == null)
                    {
                        throw new Exception("撈取託運單失敗");
                    }

                    //入DB
                    foreach (var p in PelicanScanInfo)
                    {
                        try
                        {
                            //是逆物流跟PBFA 跳過
                            if (!(!p.IsD && p.StatusCode.Equals("PBFA")))
                            {

                                if (string.IsNullOrEmpty(p.CheckNumber))
                                {
                                    throw new Exception(p.PelicanCheckNumber + " : 無託運單號");
                                }

                                var pelican_check_number = p.PelicanCheckNumber;
                                var handy_time = DateTime.Parse(p.ScanDate);
                                var status_code = p.StatusCode;
                                DateTime dateTime = DateTime.Now;

                                //檢查 有重複就跳過
                                var info = _pelican_return_content_DA.GetPelican_requestByrequest_id(handy_time, pelican_check_number, status_code);

                                if (info != null)
                                {
                                    throw new Exception(info.pelican_check_number + " : 貨態重複");
                                }


                                //判斷是否已經有存在配達貨況，如果有不再寫入

                                Pelican_request_DA _pelican_Request_DA = new Pelican_request_DA();
                                string LastScanItem = _pelican_Request_DA.GetttDeliveryScanLogLastScanItem(p.CheckNumber);

                                //未配達才寫入
                                if (string.IsNullOrEmpty(LastScanItem))
                                {
                                    //寫入DeliveryScanLog
                                    ttDeliveryScanLog_Condition ttDeliveryScanLog_Condition = new ttDeliveryScanLog_Condition();

                                    ttDeliveryScanLog_Condition.driver_code = p.DriverCode;
                                    ttDeliveryScanLog_Condition.check_number = p.CheckNumber;
                                    ttDeliveryScanLog_Condition.scan_date = handy_time;
                                    ttDeliveryScanLog_Condition.scan_item = p.ScanItem;
                                    ttDeliveryScanLog_Condition.area_arrive_code = p.AreaArriveCode;
                                    ttDeliveryScanLog_Condition.pieces = 1;
                                    ttDeliveryScanLog_Condition.weight = 0;
                                    ttDeliveryScanLog_Condition.runs = 1;
                                    ttDeliveryScanLog_Condition.plates = 0;
                                    ttDeliveryScanLog_Condition.cdate = dateTime;
                                    ttDeliveryScanLog_Condition.Del_status = 0;

                                    //逆物流
                                    if (!p.IsD)
                                    {
                                        ttDeliveryScanLog_Condition.receive_option = p.ArriveOption;

                                    }
                                    else if (p.ScanItem == "7")//正物流發送
                                    {
                                        ttDeliveryScanLog_Condition.send_option = p.ArriveOption;
                                    }
                                    else //其他
                                    {
                                        ttDeliveryScanLog_Condition.arrive_option = p.ArriveOption;
                                    }


                                    ttDeliveryScanLog_Condition.cdate = dateTime;

                                    var id = _ttDeliveryScanLog_DA.InsertReId(ttDeliveryScanLog_Condition);

                                    Pelican_return_content_Condition pelican_Return_Content_Condition = new Pelican_return_content_Condition();

                                    pelican_Return_Content_Condition.scan_log_id = id;
                                    pelican_Return_Content_Condition.write_in_time = dateTime;
                                    pelican_Return_Content_Condition.return_filename = SqlStringRegular(p.ReturnFilename);
                                    pelican_Return_Content_Condition.account_code = p.AccountCode;
                                    pelican_Return_Content_Condition.pelican_check_number = p.PelicanCheckNumber;
                                    pelican_Return_Content_Condition.fse_check_number = p.CheckNumber;
                                    pelican_Return_Content_Condition.receive_zip3 = p.ReceiveZip3;
                                    pelican_Return_Content_Condition.information_create_date = DateTime.Parse(p.InformationCreateDate);
                                    pelican_Return_Content_Condition.receive_date = string.IsNullOrEmpty(p.ReceiveDate) ? DateTime.Parse("1900-01-01 00:00:00.000") : DateTime.Parse(p.ReceiveDate);
                                    pelican_Return_Content_Condition.return_file_create_time = DateTime.Parse(p.ReturnFileCreateTime);
                                    pelican_Return_Content_Condition.end_code = p.EndCode;
                                    pelican_Return_Content_Condition.status_code = p.StatusCode;
                                    pelican_Return_Content_Condition.scan_station_code = p.ScanStationCode;
                                    pelican_Return_Content_Condition.scan_station_shortname = p.ScanStationShortname;
                                    pelican_Return_Content_Condition.handy_time = DateTime.Parse(p.ScanDate);
                                    pelican_Return_Content_Condition.status_chinese = p.StatusChinese;
                                    pelican_Return_Content_Condition.receive_code = p.ReceiveCode;
                                    pelican_Return_Content_Condition.abnormal_status_code = p.AbnormalStatusCode;
                                    pelican_Return_Content_Condition.abnormal_status_chinese = p.AbnormalStatusChinese;

                                    _pelican_return_content_DA.Insert(pelican_Return_Content_Condition);
                                }
                                else { 
                                
                                }
                            }

                        }



                        catch (Exception e)
                        {
                            File.AppendAllText(exceptionLogFolderFile, file.Name + " : " + e.Message + "   " + DateTime.Now.ToString() + "\r\n");
                        }

                    }

                    //以上都沒問題移動檔案
                    if (!Directory.Exists(ftpFileReturnRecord))
                    {
                        Directory.CreateDirectory(ftpFileReturnRecord);
                    }

                    if (!Directory.Exists(ftpFileReturnRecord))
                    {
                        Directory.CreateDirectory(ftpFileReturnRecord);
                    }
                    File.Move(ftpReturnPath + file.Name, ftpFileReturnRecord + file.Name.Replace(".txt", "_" + DateTime.Now.ToString("HHmm") + ".txt"));
                }
                catch (Exception e)
                {
                    File.AppendAllText(exceptionLogFolderFile, "Exception Message: " + e.Message + "   " + DateTime.Now.ToString() + "\r\n");
                    File.AppendAllText(exceptionLogFolderFile, "Exception StackTrace: " + e.StackTrace + "   " + DateTime.Now.ToString() + "\r\n");
                    File.AppendAllText(exceptionLogFolderFile, "Exception Source: " + e.Source + "   " + DateTime.Now.ToString() + "\r\n");


                    //推播
                    _Notification_DAL.Insert("Pelican失敗", e.Message);
                }

            }

            _log.WriteDbLog(5, isSuccess, true, logInsertId);
            _log.WriteDbLog(3, isSuccess, true, insertId);

        }

        public void FtpUploaderToPelican()
        {
            bool isSuccess = true;
            int insertId = _log.WriteDbLog(4, false, false);
            byte[] data;
            DirectoryInfo dirs = new DirectoryInfo(pathSendToPelicanTxt);
            FileInfo[] files = dirs.GetFiles("*.txt");

            for (var i = 0; i < files.Length; i++)
            {
                bool isUploadSuccess = true;
                string ftpUriUploadShunt = "";
                if (files[i].Name.EndsWith("R.txt"))            //這裡是來回件上傳
                {
                    ftpUriUploadShunt = ftpUriUpload + ftpUriUploadReturn;
                }
                else if (files[i].Name.EndsWith("Q.txt"))
                {
                    ftpUriUploadShunt = ftpUriUpload + ftpUriUploadRoundTripReturn;
                }
                else
                {
                    ftpUriUploadShunt = ftpUriUpload;
                }

                if (!Directory.Exists(pathSendToPelicanTxtUploaded))
                {
                    Directory.CreateDirectory(pathSendToPelicanTxtUploaded);
                }

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (StreamReader sourceStream = new StreamReader(pathSendToPelicanTxt + files[i].Name, Encoding.GetEncoding("big5")))
                {
                    data = Encoding.GetEncoding("big5").GetBytes(sourceStream.ReadToEnd().ToString());
                    WebClient wc = new WebClient();
                    wc.Credentials = new NetworkCredential(ftpAccountUpload, ftpPasswordUpload);
                    try
                    {
                        wc.UploadData(ftpUriUploadShunt + files[i].Name, data);     //成功的話移開到別的資料夾
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        isUploadSuccess = false;
                        _log.ExceptionRecord(4, e);
                    }
                }

                if (isUploadSuccess)
                {
                    try
                    {
                        File.Move(pathSendToPelicanTxt + files[i].Name, pathSendToPelicanTxtUploaded + files[i].Name);
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        _log.ExceptionRecord(4, e);
                    }
                }
            }

            _log.WriteDbLog(4, isSuccess, true, insertId);
        }

        public void FtpCatchFromPelican()
        {

            bool isSuccess = true;
            int insertId = _log.WriteDbLog(6, false, false);

            string lastSuccessTime;     //格式yyyyMM_dd
            string cmdText = @"select start_time from Pelican_schedule_log with(nolock) where step = 6 and is_success = 1";
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            lastSuccessTime = DateTime.Now.ToString("yyyyMM_dd");
                        }
                        else
                        {
                            while (reader.Read())
                            {
                                lastSuccessTime = ((DateTime)reader["start_time"]).ToString("yyyyMM_dd");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    _log.ExceptionRecord(6, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            //以上這段也要依照回覆檔產生形式決定怎麼抓檔案
            //是否舊檔覆蓋新檔(重複寫入資料庫)，是否完整抓取資料庫，是否會產生空檔案，已完成..吧?

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            WebClient wc = new WebClient();
            wc.Credentials = new NetworkCredential(ftpAccountDownload, ftpPasswordDownload);
            string content = "";
            string fileName = string.Format(@"8362991401_{0}.txt", DateTime.Now.ToString("yyyyMM_dd"));
            try
            {
                byte[] file = wc.DownloadData(ftpUriDownload + fileName);
                content = Encoding.GetEncoding("big5").GetString(file);
                //
                if (!File.Exists(ftpReturnPath + fileName))
                {
                    var newFile = File.Create(ftpReturnPath + fileName);
                    newFile.Close();
                }
                using (StreamWriter sw = new StreamWriter(ftpReturnPath + fileName, false, Encoding.GetEncoding("big5")))       //第二個參數決定是否覆蓋
                {
                    sw.Write(content);
                }
            }
            catch (Exception e)
            {
                isSuccess = false;
                _log.ExceptionRecord(6, e);
            }

            //以下抓取逆物流ftp

            string fileNameReturn = string.Format(@"R8362991401_{0}.txt", DateTime.Now.ToString("yyyyMM_dd"));
            try
            {
                byte[] file = wc.DownloadData(ftpUriDownload + fileNameReturn);
                content = Encoding.GetEncoding("big5").GetString(file);
                //
                if (!File.Exists(ftpReturnPath + fileNameReturn))
                {
                    var newFile = File.Create(ftpReturnPath + fileNameReturn);
                    newFile.Close();
                }
                using (StreamWriter sw = new StreamWriter(ftpReturnPath + fileNameReturn, false, Encoding.GetEncoding("big5")))       //第二個參數決定是否覆蓋
                {
                    sw.Write(content);
                }
            }
            catch (Exception e)
            {
                isSuccess = false;
                _log.ExceptionRecord(6, e);
            }

            _log.WriteDbLog(6, isSuccess, true, insertId);
        }

        private static byte[] CatchByteArrayRangeByIndexRange(byte[] array, int start, int length)     //包含start
        {
            byte[] result = new byte[length];
            for (var i = 0; i < length; i++)
            {
                result[i] = array[start + i];
            }
            return result;
        }

        private static ScanItemAndArriveOptionEntity ReturnSA(string endCode, string statusCode, string statusCodeDetail, List<PelicanStatusEntity> statusEntities, bool isD)
        {
            IEnumerable<PelicanStatusEntity> statuPaired = statusEntities.Where(x => x.PelicanStatusCode == statusCodeDetail && ((x.IsDeliveryD && isD) || (x.IsDeliveryR && !isD)));
            if (statuPaired.Count() > 0)
            {
                PelicanStatusEntity status = statuPaired.FirstOrDefault();
                ScanItemAndArriveOptionEntity result = new ScanItemAndArriveOptionEntity { ScanItem = status.ScanItem, ArriveOption = status.ArriveOption };
                return result;
            }
            else
            {
                PelicanStatusEntity status = statusEntities.Where(x => x.PelicanStatusCode == statusCode && x.EndCode == endCode && ((x.IsDeliveryD && isD) || (x.IsDeliveryR && !isD))).FirstOrDefault();
                if (status == null)
                {
                    return new ScanItemAndArriveOptionEntity { ScanItem = "", ArriveOption = "" };
                }
                ScanItemAndArriveOptionEntity result = new ScanItemAndArriveOptionEntity { ScanItem = status.ScanItem, ArriveOption = status.ArriveOption };
                return result;
            }
        }

        public bool CreateFtpFile(string result, int sheduleLogId, int fileType)            //type-->0:正物流，1:逆物流，2:來回件(去件)，3:來回件(回件)
        {

            bool isSuccess = true;
            if (result.Length > 0)
            {
                string fileName = pathSendToPelicanTxt + JFCustomerCode;
                switch (fileType)
                {
                    case 0:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
                        break;
                    case 1:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "R" + ".txt";
                        break;
                    case 2:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "P" + ".txt";
                        break;
                    case 3:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "Q" + ".txt";
                        break;
                }


                if (!File.Exists(fileName))
                {
                    var newFile = File.Create(fileName);
                    newFile.Close();
                }
                //File.AppendAllLines(fileName, resultList.ToArray());
                using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.GetEncoding("big5")))
                {
                    sw.Write(result);
                }

                //以下將新增txt檔案檔名寫入schedulelog的對應欄位
                string DbColumnName = "";
                switch (fileType)
                {
                    case 0:
                        DbColumnName = "file_name_D";
                        break;
                    case 1:
                        DbColumnName = "file_name_R";
                        break;
                    case 2:
                        DbColumnName = "file_name_P_D";
                        break;
                    case 3:
                        DbColumnName = "file_name_P_R";
                        break;
                }

                string cmdUpdateFileText = string.Format(@"update {0} set {3} = '{1}' where id = {2}", logPelicanSchedule, fileName, sheduleLogId, DbColumnName);
                using (SqlCommand cmdUpdateFile = new SqlCommand(cmdUpdateFileText))
                {
                    try
                    {
                        cmdUpdateFile.Connection = new SqlConnection(AWSConnectionString);
                        cmdUpdateFile.Connection.Open();
                        cmdUpdateFile.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        _log.ExceptionRecord(2, e);
                    }
                    finally
                    {
                        cmdUpdateFile.Connection.Close();
                    }
                }
            }
            return isSuccess;
        }
    }

}

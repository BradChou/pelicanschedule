﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Pelican.BL;
using Pelican.DAL.Model.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Pelican.Test
{
    class Program
    {
        static IConfiguration config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .Build();

        public readonly static string AWSConnectionString = config.GetConnectionString("AWSConnectionString");

        public readonly static string logPelicanSchedule = config["logPelicanSchedule"];
        public readonly static string scheduleStartDate = config["scheduleStartDate"];
        public readonly static string LYName = config["LYName"];
        public readonly static string LYCity = config["LYCity"];
        public readonly static string LYArea = config["LYArea"];
        public readonly static string LYAddress = config["LYAddress"];
        public readonly static string LYTel = config["LYTel"];
        public readonly static string toPelicanFormatDbTable = config["toPelicanFormatDbTable"];
        public readonly static int failTimes = int.Parse(config["failTimes"]);
        public readonly static int alertIfCheckNumberCountLessThanTypeD = int.Parse(config["alertIfCheckNumberCountLessThanTypeD"]);
        public readonly static int alertIfCheckNumberCountLessThanTypeR = int.Parse(config["alertIfCheckNumberCountLessThanTypeR"]);
        public readonly static string TcDeliveryRequest = config["TcDeliveryRequest"];
        public readonly static string requestPelican = config["requestPelican"];
        public readonly static string intermodalTransportationCheckNumber = config["intermodalTransportationCheckNumber"];
        public readonly static string companyIdPelican = config["companyIdPelican"];
        public readonly static string logPelicanFailInsertIntoPelicanRequest = config["logPelicanFailInsertIntoPelicanRequest"];
        public readonly static string ftpReturnPath = config["ftpReturnPath"];
        public readonly static string pelicanStatusTable = config["pelicanStatusTable"];
        public readonly static string JFCustomerCode = config["JFCustomerCode"];
        public readonly static string scanLogDriverCode = config["scanLogDriverCode"];
        public readonly static string PelicanReturnContent = config["PelicanReturnContent"];
        public readonly static string ftpFileReturnRecord = config["ftpFileReturnRecord"];
        public readonly static string pathSendToPelicanTxt = config["pathSendToPelicanTxt"];
        public readonly static string ftpUriUpload = config["ftpUriUpload"];
        public readonly static string ftpUriUploadReturn = config["ftpUriUploadReturn"];
        public readonly static string ftpUriUploadRoundTripReturn = config["ftpUriUploadRoundTripReturn"];
        public readonly static string pathSendToPelicanTxtUploaded = config["pathSendToPelicanTxtUploaded"];
        public readonly static string ftpAccountUpload = config["ftpAccountUpload"];
        public readonly static string ftpPasswordUpload = config["ftpPasswordUpload"];
        public readonly static string ftpAccountDownload = config["ftpAccountDownload"];
        public readonly static string ftpPasswordDownload = config["ftpPasswordDownload"];
        public readonly static string ftpUriDownload = config["ftpUriDownload"];

        public readonly static string exceptionLogFileName = config["exceptionLogFileName"];
        public readonly static string exceptionLogFolder = config["exceptionLogFolder"];

        static void Main(string[] args)
        {
//            PelicanBL _PelicanBL = new PelicanBL();

//            using (var cn = new SqlConnection(AWSConnectionString))
//            {
//                string sql = @"
//SELECT *
//FROM tcDeliveryRequests d WITH (NOLOCK)
//LEFT JOIN ttDeliveryRequestsRecord r WITH (NOLOCK) ON d.request_id = r.request_id
//WHERE d.check_number = '700049735431'

//";
//                var sr = cn.QueryFirst<dynamic>(sql);

//                List<DeliveryRequestEntity> deliveryRequestEntities = new List<DeliveryRequestEntity>();

              

//                var aaaaa = sr.arrive_to_pay_append.ToString();
//                DeliveryRequestEntity entity = new DeliveryRequestEntity()
//                {
//                    JFRequestId = sr.request_id.ToString(),
//                    JFCheckNumber = sr.check_number.ToString(),
//                    OrderNumber = sr.order_number.ToString(),
//                    Pieces = sr.pieces.ToString() == "" ? "1" : sr.pieces.ToString(),
//                    SendContact = LYName,
//                    SendTel = LYTel,
//                    SendCity = LYCity,
//                    SendArea = LYArea,
//                    SendAddress = LYAddress,
//                    ReceiveContact = sr.receive_contact.ToString(),
//                    ReceiveTel = sr.receive_tel1.ToString(),
//                    ReceiveCity = sr.receive_city.ToString(),
//                    ReceiveArea = sr.receive_area.ToString(),
//                    ReceiveAddress = sr.receive_address.ToString(),
//                    ArriveAssignDate = sr.arrive_assign_date.ToString() == "" ? sr.arrive_assign_date.ToString() : DateTime.Parse(sr.arrive_assign_date.ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
//                    AssignedTimePeriod = PelicanBL.DefineMorningOrAfternoon(sr.time_period.ToString()),
//                    CollectionMoney = sr.collection_money.ToString() == "" ? "0" : sr.collection_money.ToString(),
//                    DeliveryType = sr.DeliveryType.ToString(),
//                    Remark = sr.invoice_desc.ToString(),
//                    RoundTrip = sr.round_trip.ToString(),
//                    DrRequestId = sr.request_id.ToString(),
//                    RoundTripCheckNumber = sr.roundtrip_checknumber == null ? "" : sr.roundtrip_checknumber.ToString(),
//                };
//                deliveryRequestEntities.Add(entity);
//                List<PostZipEntity> zipList = new List<PostZipEntity>();
//                string cmdZipText = @"select * from ttArriveSites with(nolock)";
//                using (SqlCommand cmd = new SqlCommand(cmdZipText))
//                {
//                    cmd.Connection = new SqlConnection(AWSConnectionString);
//                    try
//                    {
//                        cmd.Connection.Open();
//                        using (SqlDataReader srr = cmd.ExecuteReader())
//                        {
//                            while (srr.Read())
//                            {
//                                PostZipEntity pzEntity = new PostZipEntity()
//                                {
//                                    PostArea = srr["post_area"].ToString(),
//                                    PostCity = srr["post_city"].ToString(),
//                                    Zip = srr["zip"].ToString()
//                                };
//                                zipList.Add(pzEntity);
//                            }

//                        }
//                    }
//                    catch (Exception e)
//                    {

//                    }
//                    finally
//                    {
//                        cmd.Connection.Close();
//                    }
//                }

//                for (var i = 0; i < deliveryRequestEntities.Count; i++)
//                {
//                    try
//                    {
//                        PelicanCreateReuturnEntity apiReturnEntity = PelicanBL.ApiRequestToPelican(deliveryRequestEntities[i].ReceiveCity + deliveryRequestEntities[i].ReceiveArea + deliveryRequestEntities[i].ReceiveAddress, deliveryRequestEntities[i].ReceiveZip3);
//                        deliveryRequestEntities[i].IsNeedToInsertTable = apiReturnEntity.IsSuccess;

//                        if (deliveryRequestEntities[i].IsNeedToInsertTable)     //若沒有取得宅配通郵遞區號則不寫入資料表
//                        {
//                            JfCustomerCodeAndPelicanCheckNumber temp = deliveryRequestEntities[i].DeliveryType == "D" ? new PelicanBL().GetPelicanCheckNumber(false, false, deliveryRequestEntities[i].JFCheckNumber) : new PelicanBL().GetPelicanCheckNumber(true, deliveryRequestEntities[i].RoundTrip.ToLower() == "true", deliveryRequestEntities[i].JFCheckNumber);
//                            deliveryRequestEntities[i].PelicanCheckNumber = temp.PelicanCheckNumber.PadLeft(12, ' ');
//                            deliveryRequestEntities[i].JfCustomerCode = temp.JfCustomerCode;
//                            deliveryRequestEntities[i].SendZip3 = PelicanBL.FromAddressToZip(deliveryRequestEntities[i].SendCity, deliveryRequestEntities[i].SendArea, zipList);
//                            deliveryRequestEntities[i].ReceiveZip3 = PelicanBL.FromAddressToZip(deliveryRequestEntities[i].ReceiveCity, deliveryRequestEntities[i].ReceiveArea, zipList);

//                            deliveryRequestEntities[i].PelicanReceiveZip5 = apiReturnEntity.PZip5;
//                            deliveryRequestEntities[i].PelicanReceiveArea = apiReturnEntity.Area;
//                            deliveryRequestEntities[i].Remark = (deliveryRequestEntities[i].RoundTrip.ToLower() == "true" ? "回件:" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(0, 1) + "84" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(3, 9) : "") + deliveryRequestEntities[i].Remark;
//                        }

//                    }
//                    catch (Exception e)
//                    {

//                    }

//                }

               

//                _PelicanBL.TraslateAndExportToTxt(deliveryRequestEntities, null);


//            }

            Console.WriteLine("Hello World!");
        }
    }
}
